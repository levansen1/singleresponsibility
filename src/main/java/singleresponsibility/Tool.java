/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package singleresponsibility;

import java.util.Calendar;
import java.util.Date;


public class Tool {
       
    public boolean isPromotionDueThisYear(Employee employee,boolean goodPerformance){
        
        Calendar dateJoined =  Calendar.getInstance();
        dateJoined.setTime(employee.getDateOfJoining());
        dateJoined.add(Calendar.YEAR, 1);
        Calendar today = Calendar.getInstance();
        
        return today.after(dateJoined)&& goodPerformance;
        
        
        
    }
    
   public double calcIncomeTaxForCurrentYear(Employee employee,double taxPercentage){
       return employee.getEmployeeSalary()*taxPercentage;
   }
   
}
