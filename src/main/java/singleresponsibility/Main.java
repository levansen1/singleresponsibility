/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package singleresponsibility;

import java.util.Calendar;

/**
 *
 * @author levansen
 */
public class Main {
   public static void main(String[] args){
        Tool tools = new Tool();
       Calendar dateJoin = Calendar.getInstance();
       dateJoin.set(2021, 0, 22);
       Employee emp1 = new Employee("001",dateJoin.getTime(),40000);

       System.out.println("Is promotion due " + tools.isPromotionDueThisYear(emp1, true));
       System.out.println("Taxes for the year"+ tools.calcIncomeTaxForCurrentYear(emp1, 0.28));
       
       
   }
 
}
